The following external dependencies have been included in this repository for convenience:

* websocketpp v0.7.0 from https://github.com/zaphoyd/websocketpp/tree/0.7.0 (custom permissive licence)
* ASIO standalone v1.10.8 from https://think-async.com/ (boost licence)
* rapidjson v1.1.0 from https://github.com/Tencent/rapidjson (MIT licence)
* Simple-Web-Server v3.0.0-rc2 from https://github.com/eidheim/Simple-Web-Server (MIT licence)

In some cases a lot of the unused parts have been stripped out (e.g. the 25M ASIO doc directory), but
licence and legal documents should all be present.

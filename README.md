# MP4Server

Listens on a given port for requests for video files. When a request arrives, converts raw H264 files
on disk into an MP4 file and serves over the network.

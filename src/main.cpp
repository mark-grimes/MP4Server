#include <cstdio>
#include <isobmff/MP4Writer.h>
#include <server_http.hpp>
#include "mp4server/FileList.h"
#include "mp4server/version.h"

class ResponseWrapper
{
public:
	ResponseWrapper( SimpleWeb::Server<SimpleWeb::HTTP>::Response& response, size_t maxSize=4096 ) : response_(response) {}

	template<typename T>
	void write( T&& data )
	{
		response_.write( reinterpret_cast<char *>(&data), sizeof(data) );
	}

	void writeBytes( void* pData, size_t size )
	{
		response_.write( reinterpret_cast<char *>(pData), size );
	}

protected:
	SimpleWeb::Server<SimpleWeb::HTTP>::Response& response_;
};

#include "isobmff/ByteInterface.h"

template<>
struct isobmff::ByteInterface<ResponseWrapper&>
{
	template<size_t pos,typename T>
	static constexpr void set( ResponseWrapper& file, T&& instance )
	{
		file.write( isobmff::detail::ByteOrder<T>::hostToNetwork(instance) );
	}
	template<typename T>
	static constexpr void set( ResponseWrapper& file, T&& instance, size_t pos )
	{
		file.write( isobmff::detail::ByteOrder<T>::hostToNetwork(instance) );
	}
};

template<typename Container>
void recursiveWriteBody( std::shared_ptr<SimpleWeb::Server<SimpleWeb::HTTP>::Response> response, std::shared_ptr<isobmff::H264Parser> pParser, typename Container::iterator current, std::shared_ptr<Container> pContainer )
{
	if( current!=pContainer->end() )
	{
		printf( "\t serving %s\n", current->c_str() );
		isobmff::MP4Writer::writeBody( *pParser, current, current+1, ResponseWrapper(*response) );
		response->send( [response,pParser,current,pContainer]( const std::error_code& error ){
			if( error ) fprintf( stderr, "Got error %s while writing file contents\n", error.message().c_str() );
			else recursiveWriteBody( response, pParser, current+1, pContainer );
		} );
	}
	else isobmff::MP4Writer::writeTail( *pParser, 20, ResponseWrapper(*response) );
}

int main( int argc, char* argv[] )
{
	// Current version of ISOBMFF can't detect system endianess, so quit if this is done incorrectly.
	{ // block to limit scope of temporary variables
		uint32_t endianTest=isobmff::detail::ByteOrder<uint32_t>::hostToNetwork( 0x01020304 );
		char* byteArray=reinterpret_cast<char*>( &endianTest );
		if( byteArray[0]!=0x01 || byteArray[1]!=0x02 || byteArray[2]!=0x03 || byteArray[3]!=0x04 )
		{
			fprintf(stderr, "ERROR! The current system does not have a supported endian format\n" );
			return -1;
		}
	}

	std::string rootDirectory("."); // Default to current directory if no directory specified
	if( argc>1 ) rootDirectory=argv[1];
	rootDirectory=mp4server::canonicalPath(rootDirectory);

	printf( "MP4Server version %s\nServing from path '%s'\n", mp4server::version::GitDescribe, rootDirectory.c_str() );

	try
	{
		using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;
		HttpServer testServer;
		testServer.config.port = 8080;
		// testServer.config.thread_pool_size = 2;
		testServer.default_resource["GET"] = [&rootDirectory](std::shared_ptr<HttpServer::Response> response,std::shared_ptr<HttpServer::Request> request) {
			// This file list needs to stay alive in between callbacks, so use shared_ptr
			std::shared_ptr<std::vector<std::string>> pFileList=std::make_shared<std::vector<std::string>>();
			printf( "Request for path '%s'\n", request->path.c_str() );
			try{ mp4server::listFiles( rootDirectory+request->path, *pFileList, ".h264" ); }
			catch( const std::exception& error ){
				*response << "HTTP/1.1 400 Bad Request\r\nContent-Length: " << strlen(error.what()) << "\r\n\r\n" << error.what();
				return;
			}
			if( pFileList->empty() )
			{
				const std::string noFilesError="No files found";
				*response << "HTTP/1.1 400 Bad Request\r\nContent-Length: " << noFilesError.size() << "\r\n\r\n" << noFilesError;
				return;
			}
			std::sort( pFileList->begin(), pFileList->end() );
			for( const auto& path : *pFileList ) printf( "\t%s\n", path.c_str() );
			// size_t contentLength=0;
			// for( const auto& path : *pFileList ) contentLength+=path.size()+1;
			// *response << "HTTP/1.0 200 OK\r\n"
			// 	"Content-Length: " << contentLength << "\r\n"
			// 	"\r\n";
			// for( const auto& path : *pFileList ) *response << path << "\n";
			// return;
			size_t fileSize;
			size_t parseLimit;
			{ // Block to limit scope of temporary variables
				isobmff::H264Parser tempParser;
				isobmff::MP4Writer::parseFiles( tempParser, pFileList->begin(), pFileList->end() );
				fileSize=isobmff::MP4Writer::fileSize(tempParser);
				parseLimit=tempParser.position;
			}
			printf( "Full file size will be %zu\n", fileSize );
			*response << "HTTP/1.0 200 OK\r\n"
				"Content-Length: " << fileSize << "\r\n"
				"Content-Type: video/mp4\r\n"
				"Content-Disposition: attachment; filename=\"ethoscopeVideo.mp4\"\r\n"
				"\r\n";
			// response->close_connection_after_response=true;
			ResponseWrapper responseWrapper( *response );
			auto pParser=std::make_shared<isobmff::H264Parser>();
			isobmff::MP4Writer::writeHead( pFileList->begin(), pFileList->end(), responseWrapper );
			response->send( [response,pParser,pFileList]( const std::error_code& error ){
				if( error ) fprintf( stderr, "Got error %s while writing file header\n", error.message().c_str() );
				else recursiveWriteBody( response, pParser, pFileList->begin(), pFileList );
			} );
		};
		testServer.on_error = []( std::shared_ptr<HttpServer::Request> request, const SimpleWeb::error_code& ec ){
			if( ec ) fprintf( stderr, "Got server error %s\n", ec.message().c_str() );
		};
		testServer.start();
	}
	catch( const std::exception& error )
	{
		std::cerr << "Exception: " << error.what() << "\n";
		return -1;
	}

	return 0;
}

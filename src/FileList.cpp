#include "mp4server/FileList.h"
#include <dirent.h>
#include <sys/stat.h>
#include <stdexcept>
#include <cstring>

/** @brief Exception safe holder for a dirent.h DIR structure */
class DirectorySentry
{
public:
	DirectorySentry( char const* path ) : pDirectory_( opendir( path ) )
	{
		if( pDirectory_==nullptr ) throw std::invalid_argument("Invalid path");
	}
	~DirectorySentry() { closedir(pDirectory_); }
	operator DIR*() { return pDirectory_; }
protected:
	DIR* pDirectory_;
};

bool mp4server::detail::stringEndsWith( const std::string& string, const std::string& requiredSuffix )
{
	if( requiredSuffix.empty() ) return true;
	if( string.size()<requiredSuffix.size() ) return false;
	if( string.rfind(requiredSuffix)==string.size()-requiredSuffix.size() ) return true;
	else return false;
}

std::string mp4server::canonicalPath( const std::string& path )
{
	char actualPath[PATH_MAX+1];
	char* pResult=realpath( path.c_str(), actualPath );
	if( pResult==nullptr ) throw std::invalid_argument("Couldn't get canonicalPath for '"+path+"'");
	else return pResult;
}

void mp4server::listFiles( const std::string& path, std::vector<std::string>& results, const std::string& requiredSuffix )
{
	DirectorySentry directory( path.c_str() );
	struct dirent *pEntry;
	while( (pEntry=readdir(directory)) != nullptr )
	{
		if( strcmp(pEntry->d_name,".")!=0 && strcmp(pEntry->d_name,"..")!=0 )
		{
			struct stat stat_buf;
			std::string fullPath;
			if( path.back()!='/' ) fullPath=path+"/"+pEntry->d_name;
			else fullPath=path+pEntry->d_name;
			if( stat(fullPath.c_str(),&stat_buf)==0 )
			{
				if( S_ISDIR(stat_buf.st_mode) ) listFiles( fullPath, results, requiredSuffix );
				else if( detail::stringEndsWith(fullPath,requiredSuffix) ) results.emplace_back( std::move(fullPath) );
			}

		}
	}
}

#pragma once
#include <vector>
#include <string>

namespace mp4server
{
	namespace detail
	{
		bool stringEndsWith( const std::string& string, const std::string& requiredSuffix );
	}
	std::string canonicalPath( const std::string& path );
	void listFiles( const std::string& path, std::vector<std::string>& results, const std::string& requiredSuffix="" );
	// inline std::vector<std::string> listFiles( const std::string& path ) { return listFiles(path.c_str()); }
} // end of namespace mp4server

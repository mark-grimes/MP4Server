#ifndef INCLUDEGUARD_mp4server_version_h
#define INCLUDEGUARD_mp4server_version_h

#include <iosfwd>

namespace mp4server
{
	namespace version
	{
		extern const char* GitDescribe;
		extern const char GitHash[41];

	} // end of namespace version
} // end of namespace mp4server

#endif

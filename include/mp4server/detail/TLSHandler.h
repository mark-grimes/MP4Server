#ifndef ENCRYPTED_WEBSOCKETS_AVAILABLE
#	warning "TLSHandler cannot be used if OpenSSL is not part of the project"
#else

#ifndef INCLUDEGUARD_mp4server_impl_TLSHandler_h
#define INCLUDEGUARD_mp4server_impl_TLSHandler_h

#include <functional>
#include <string>
#include <websocketpp/config/asio.hpp>

namespace mp4server
{

	namespace detail
	{
		/** @brief Methods common to Client and Server that deal with the TLS handshake.
		 *
		 * @author Mark Grimes
		 * @date 26/Jul/2015
		 */
		class TLSHandler
		{
		public:
			TLSHandler( websocketpp::config::asio::alog_type& logger );

			void setCertificateChainFile( const std::string& filename );
			void setCertificateChainFile( std::string&& filename );
			void setPrivateKeyFile( const std::string& filename );
			void setPrivateKeyFile( std::string&& filename );
			void setVerifyFile( const std::string& filename );
			void setVerifyFile( std::string&& filename );
			void setDiffieHellmanParamsFile( const std::string& filename );
			void setDiffieHellmanParamsFile( std::string&& filename );
			void setCertificateVerificationCallback( std::function<bool(bool,websocketpp::lib::asio::ssl::verify_context&)>&& function );
			void setPasswordCallback( std::function<std::string(size_t,websocketpp::lib::asio::ssl::context::password_purpose)>&& function );

			std::shared_ptr<websocketpp::lib::asio::ssl::context> handle_tls_init_intermediate( websocketpp::connection_hdl hdl ) const;
			std::shared_ptr<websocketpp::lib::asio::ssl::context> handle_tls_init_modern( websocketpp::connection_hdl hdl ) const;
		protected:
			void handle_tls_init_common( std::shared_ptr<websocketpp::lib::asio::ssl::context> pSSLContext ) const;
			std::string certificateChainFileName_;
			std::string privateKeyFileName_;
			std::string verifyFileName_;
			std::string diffieHellmanParamsFileName_;
			std::function<bool(bool,websocketpp::lib::asio::ssl::verify_context&)> certificateVerificationCallback_;
			std::function<std::string(size_t,websocketpp::lib::asio::ssl::context::password_purpose)> passwordCallback_;
			websocketpp::config::asio::alog_type& logger_;
		};

	} // end of namespace detail
} // end of namespace mp4server

#endif // end of "#ifndef INCLUDEGUARD_mp4server_impl_TLSHandler_h"
#endif // end of ifndef "#ifdef ENCRYPTED_WEBSOCKETS_AVAILABLE"

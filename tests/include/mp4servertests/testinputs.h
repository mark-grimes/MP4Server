/** @file Information required by all tests (locations of input files etcetera). */
#include <string>

namespace mp4servertests
{
	struct testinputs
	{
		static const std::string testFileDirectory;
	};
} // end of namespace mp4servertests

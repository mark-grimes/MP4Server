#pragma once

namespace mp4servertests
{
	/** @brief Class to help checking test results stored in containers.
	 *
	 * With this class it's possible to write simple Catch.hpp tests e.g.
	 *
	 * @begincode
	 * std::vector<int> results;
	 * // ... run code that fills results
	 * CHECK( SequenceComparator<int>{5,9,8}==results );
	 * @endcode
	 *
	 * instead of
	 *
	 * @begincode
	 * std::vector<int> results;
	 * // ... run code that fills results
	 * REQUIRE( results.size()==3 );
	 * CHECK( results[0]==5 );
	 * CHECK( results[0]==9 );
	 * CHECK( results[0]==8 );
	 * @endcode
	 *
	 * For some reason class template argument deduction doesn't work inside a CHECK statement, but
	 * it does outside it (you have to specify the type). E.g. This works:
	 * @begincode
	 * // This works...
	 * auto expected=SequenceComparator{5,9,8};
	 * CHECK( expected==results );
	 * // This won't work without the template parameter
	 * CHECK( SequenceComparator<int>{5,9,8}==results );
	 * @endcode
	 * Something to do with Catch.hpp macro trickery complicating the parsing I assume.
	 *
	 * @author Mark Grimes (mark.grimes@rymapt.com)
	 * @date 03/May/2018
	 * @copyright Copyright 2018 Rymapt Ltd, licence to be decided
	 */
	template<typename T>
	class SequenceComparator
	{
	public:
		SequenceComparator( std::initializer_list<T> list ) : expectedValues_( list ) {}
		template<typename U>
		bool operator==( const U& other ) const
		{
			if( expectedValues_.size()!=other.size() ) return false;
			auto me=expectedValues_.begin();
			for( auto them=other.begin(); me!=expectedValues_.end() && them!=other.end(); ++me, ++them )
			{
				if( *me != *them ) return false;
			}
			return true;
		}
		auto begin() const { return expectedValues_.begin(); }
		auto end() const { return expectedValues_.end(); }
	protected:
		std::vector<T> expectedValues_;
	};
} // end of namespace mp4servertests

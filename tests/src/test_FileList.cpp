#include "mp4server/FileList.h"
#include "mp4servertests/testinputs.h"
#include "mp4servertests/SequenceComparator.h"
#include "catch.hpp"

SCENARIO( "Test that detial::stringEndsWith works correctly", "[FileList]" )
{
	CHECK( mp4server::detail::stringEndsWith("foobar","")==true );
	CHECK( mp4server::detail::stringEndsWith("foobar","bar")==true );
	CHECK( mp4server::detail::stringEndsWith("foobar","oba")==false );
	CHECK( mp4server::detail::stringEndsWith("fo","bar")==false );
	CHECK( mp4server::detail::stringEndsWith("bar","bar")==true );
}

SCENARIO( "Test that canonicalPath works correctly", "[FileList]" )
{
	std::string result;
	CHECK_NOTHROW( result=mp4server::canonicalPath(mp4servertests::testinputs::testFileDirectory) );
	CHECK( result==mp4server::canonicalPath(mp4servertests::testinputs::testFileDirectory) );
	CHECK_NOTHROW( result=mp4server::canonicalPath(mp4servertests::testinputs::testFileDirectory+"/tlscerts/..") );
	CHECK( result==mp4server::canonicalPath(mp4servertests::testinputs::testFileDirectory) );

	CHECK_THROWS( result=mp4server::canonicalPath(mp4servertests::testinputs::testFileDirectory+"/asdlfjkasdlk") );
}

SCENARIO( "Test that listFiles works correctly", "[FileList]" )
{
	using ExpectedResults=mp4servertests::SequenceComparator<std::string>;

	WHEN( "Checking without specifying a suffix requirement" )
	{
		std::vector<std::string> results;
		const std::string& directory=mp4servertests::testinputs::testFileDirectory;
		CHECK_NOTHROW( mp4server::listFiles( directory, results ) );
		std::sort( results.begin(), results.end() ); // Results might not be sorted, so do so to match my expected results
		CHECK( ExpectedResults{directory+"tlscerts/authorityA_cert.pem",
			directory+"tlscerts/authorityB_cert.pem",
			directory+"tlscerts/serverA_cert.pem",
			directory+"tlscerts/serverA_cert_expired.pem",
			directory+"tlscerts/serverA_key.pem",
			directory+"tlscerts/serverB_cert.pem",
			directory+"tlscerts/serverB_cert_expired.pem",
			directory+"tlscerts/serverB_key.pem"}==results );
	}
	WHEN( "Checking with a suffix requirement" )
	{
		std::vector<std::string> results;
		const std::string& directory=mp4servertests::testinputs::testFileDirectory;
		CHECK_NOTHROW( mp4server::listFiles( directory, results, std::string("cert.pem") ) );
		std::sort( results.begin(), results.end() ); // Results might not be sorted, so do so to match my expected results
		CHECK( ExpectedResults{directory+"tlscerts/authorityA_cert.pem",
			directory+"tlscerts/authorityB_cert.pem",
			directory+"tlscerts/serverA_cert.pem",
			directory+"tlscerts/serverB_cert.pem"}==results );
	}
}
